const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const PORT = process.env.PORT || 5000
const { Server } = require('socket.io')
const axios = require('axios')
const io = new Server(server, {
    cors: {
        origin: "https://tube-gether.netlify.app",
        methods: ["GET", "POST"]
    }
})


io.on("connection", (socket) => {

    socket.on("join-room", roomID => {
        socket.join(roomID)
    })

    socket.on("user-connected", (data) => {
        io.to(data.room).emit("new-user", {name: data.name, id: data.id})
    })

    socket.on("new-message", (data) => {
        io.to(data.roomID).emit("msg", data);
    })

    socket.on("play-now", (data) => {
        socket.broadcast.to(data.room).emit("play-now", {url: data.url, website: data.website, type: data.type})
    })

    socket.on("pause-video", (room) => {
        socket.broadcast.to(room).emit("pause-video")
    })

    socket.on("resume-video", (room) => {
        socket.broadcast.to(room).emit("resume-video")
    })

    socket.on("add-to-queue", (data) => {
        socket.broadcast.to(data.room).emit("add-to-queue", {link: data.link, data: data.data})
    })

    socket.on("seek-to", (data) => {
        socket.broadcast.to(data.room).emit("seek-to", data.seconds)
    })

    socket.on("play-now-from-playlist", (data) => {
        socket.broadcast.to(data.room).emit("play-now-from-playlist", {index: data.index, videoData: data.data})
    }) 

    socket.on("delete-from-playlist", (data) => {
        socket.broadcast.to(data.room).emit("delete-from-playlist", data.index)
    }) 

    socket.on("give-usernames", (data) => {
        socket.broadcast.to(data.room).emit("give-username", data)
    })

    socket.on("username", (data) => {
        socket.to(data.destination_id).emit("received-username", {name: data.name, id: data.id})
    })

    socket.on("give-room-data", (data) => {
        socket.broadcast.to(data.room).emit("give-room-data", data.id)
    })

    socket.on("room-data", (data) => {
        socket.to(data.destination_id).emit("received-room-data", data)
    })

    socket.on("give-progress", (data) => {
        console.log(data)
        socket.broadcast.to(data.room).emit("give-progress", data.id)
    })

    socket.on("progress", (data) => {
        socket.to(data.id).emit("received-progress", data.progress)
    })

    socket.on("disconnecting", () => {
        let socketRoom

        socket.rooms.forEach((item) => {
            if(item != socket.id) {
                socketRoom = item
            }
        })
        
        socket.to(socketRoom).emit("user-disconnected", socket.id)
    })
})

server.listen(PORT, () => console.log("Server listening"))